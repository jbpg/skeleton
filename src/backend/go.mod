module backend

go 1.14

require (
	github.com/Masterminds/goutils v1.1.0 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible // indirect
	github.com/andybalholm/cascadia v1.2.0 // indirect
	github.com/astaxie/beego v1.12.2
	github.com/brianvoe/gofakeit/v4 v4.3.0
	github.com/gadelkareem/cachita v0.2.1
	github.com/gadelkareem/go-helpers v0.0.0-20200725110331-77a434fc0423
	github.com/gadelkareem/gocialite v1.0.3-0.20200906110041-d3fe1179b224
	github.com/gadelkareem/que v1.0.3
	github.com/gbrlsnchs/jwt/v3 v3.0.0-rc.2
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df
	github.com/go-pg/pg/v9 v9.2.0
	github.com/google/jsonapi v0.0.0-20200825183604-3e3da1210d0c
	github.com/google/uuid v1.1.2 // indirect
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/jackc/pgx/v4 v4.8.1
	github.com/jaytaylor/html2text v0.0.0-20200412013138-3577fbdbcff7 // indirect
	github.com/lib/pq v1.8.0
	github.com/magefile/mage v1.10.0 // indirect
	github.com/matcornic/hermes/v2 v2.1.0
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mediocregopher/radix/v3 v3.5.2 // indirect
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/mitchellh/hashstructure v1.0.0
	github.com/mitchellh/reflectwalk v1.0.1 // indirect
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/pquerna/otp v1.2.0
	github.com/prometheus/common v0.13.0 // indirect
	github.com/rubenv/sql-migrate v0.0.0-20200616145509-8d140a17f351
	github.com/segmentio/encoding v0.1.17 // indirect
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/ttacon/builder v0.0.0-20170518171403-c099f663e1c2 // indirect
	github.com/ttacon/libphonenumber v1.1.0
	github.com/vanng822/go-premailer v1.9.0 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
